# Programming language chatbot

A web client for a Dialogflow chatbot.

## setup

1. git clone this repository.
2. copy `.env.example` into `.env`
3. copy `nette/app/config/local.example.neon` into `nette/app/config/local.neon`
4. set up your own dialogflow parameters in `nette/app/config/params.neon`
    - add your own credentials file for authenticating the dialogflow API and set the path in `googleCloud.credentials`
    - set the project id `googleCloud.projectId`
    - set the language code `googleCloud.languageCode`
    
launch the docker image with `docker-compose up -d`

> check that the folders `nette/temp` and `nette/log` exist and have write rights.