#!/usr/bin/env bash
# Soubor příkazů, které se provedou v docker konteineru ihned po spuštění nebo sestavení image

#
# Změny portů - pokud jsou definovány příslušné proměnné, upraví konfiguráky.
# Proměnné jsou v .env souboru a předávají se v docker-compose.yml.
#
if [ -n "$PORT_HTTP" ]; then
    echo "Nastavuji port $PORT_HTTP pro HTTP."
    sed -i "0,/Listen 80/s//Listen $PORT_HTTP/" /etc/apache2/ports.conf
    sed -i "0,/:80/s//:$PORT_HTTP/" /etc/apache2/apache2.conf
fi
if [ -n "$PORT_HTTPS" ]; then
    echo "Nastavuji port $PORT_HTTPS pro HTTPS."
    sed -i "0,/Listen 443/s//Listen $PORT_HTTPS/" /etc/apache2/ports.conf
    sed -i "0,/Listen 443/s//Listen $PORT_HTTPS/" /etc/apache2/ports.conf
    sed -i "0,/:443/s//:$PORT_HTTPS/" /etc/apache2/sites-enabled/default-ssl.conf
fi

umask 0000

#
# Uloží se aktuální adresář, aby se do něj mohl po každé akci vracet.
#
currentdir=$(pwd)

#
# composer
#
echo "---------------------------------------------------"
echo "> composer install"
cd nette/
if [ -f composer.json ]; then
    composer install
elif [ -f ../composer.json ]; then
    cd ..
    composer install
else
    echo "nebylo spusteno - composer.json nenalezen"
fi

cd $currentdir

#
# bower
#
echo "---------------------------------------------------"
echo "> bower install --allow-root"

if [ -f bower.json ]; then
    bower install --allow-root
elif [ -f ../bower.json ]; then
    cd ..
    bower install --allow-root
else
    echo "nebylo spusteno - bower.json nenalezen"
fi

cd $currentdir

#
# Zkopírování lokální konfigurace, pokud neexistuje
#
echo "---------------------------------------------------"
cd nette/
if [ -d app/config/local.example.neon ]; then
    if [ ! -d app/config/local.neon ]; then
        echo "Vytvarim app/config/local.neon kopii z 'app/config/local.example.neon'."
        cp -avr app/config/local.example.neon app/config/local.neon
    else
        echo "Adresar app/config/local.neon již existuje, neni potreba kopirovat 'local.example.neon'."
    fi
elif [ -d ../app/config/local.example.neon ]; then
    if [ ! -d ../app/config/local.neon ]; then
        echo "Vytvarim app/config/local.neon kopii z 'app/config/local.example.neon'."
        cp -avr ../app/config/local.example.neon ../app/config/local.neon
    else
        echo "Adresar app/config/local.neon již existuje, neni potreba kopirovat 'local.example.neon'."
    fi
else
    echo "app/config/local.example.neon nenalezen"
fi

echo "Zapinam webovy server! Pro ukonceni stisknete CTRL+C."

#
# Spuštění Apache
#
apache2ctl -D FOREGROUND
