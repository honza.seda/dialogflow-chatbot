FROM debian:buster-slim

# Debian setting
ENV DEBIAN_FRONTEND=noninteractive

# err fix: invoke-rc.d: policy-rc.d denied execution of start.
RUN sed -i "s/^exit 101$/exit 0/" /usr/sbin/policy-rc.d

# PACKAGE INSTALLATION
RUN apt-get update && \
    apt-get install -y \
    #
    # BASIC PACKAGES
    #
    wget curl sudo \
    #
    # DEV
    #
    git build-essential apache2 \
    #
    # PHP
    #
    php7.3 php7.3-cli libapache2-mod-php7.3 \
    #
    # PHP EXTENSIONS
    #
    php7.3-common \
    php7.3-curl \
    php7.3-gd \
    php-imagick \
    php7.3-imap \
    php7.3-intl \
    php7.3-json \
    php7.3-mbstring \
    php-redis \
    php7.3-soap \
    php7.3-xml \
    php7.3-zip \
    php7.3-bcmath

# yarn, bower install
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
RUN apt update && sudo apt install yarn -y
RUN yarn global add bower

# Apache config
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

RUN a2enmod include && \
    a2enmod rewrite && \
    a2enmod proxy_fcgi && \
    a2enmod ssl && \
    rm /etc/apache2/sites-enabled/000-default.conf

COPY ./docker/apache/apache2.conf /etc/apache2/
COPY ./docker/apache/default-ssl.conf /etc/apache2/sites-enabled/

# Set apache-created file rights to RW-RW-RW-, to prevent rights issues
RUN echo "umask 000" >>  /etc/apache2/envvars

# PHP config
COPY ./docker/php/php.ini /etc/php/7.3/apache2/
COPY ./docker/php/php.ini /etc/php/7.3/cli/

# PHP session directory rights
RUN chown www-data:www-data /var/lib/php/sessions

# composer
RUN wget https://getcomposer.org/installer && \
    php installer --install-dir=/usr/bin --filename=composer && \
    rm installer
ENV COMPOSER_ALLOW_SUPERUSER 1

# Clean-up of a cache dockerimage
RUN apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /var/lib/log/* /tmp/* /var/tmp/*

# bin scripts
COPY ./docker/bin/container.start.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Set workdir
WORKDIR /var/www

# Run the start script
RUN sed -i 's/\r$//' /usr/local/bin/container.start.sh
CMD /usr/local/bin/container.start.sh
