<?php


namespace App\Model;


use App\Enum\EDialogMessageType;
use Google\Cloud\Dialogflow\V2\EventInput;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\TextInput;
use Nette\Http\Session;
use Nette\Http\SessionSection;

class DialogflowSession
{

    const SECTION_NAME = 'DialogflowSession';
    /**
     * @var SessionSection
     */
    private $section;
    /**
     * @var SessionsClient
     */
    private $sessionsClient;
    /**
     * @var string
     */
    private $projectId;
    /**
     * @var string
     */
    private $session;

    public function __construct(
        $credentials,
        $projectId,
        $languageCode,
        Session $session
    )
    {
        $this->section = $session->getSection(self::SECTION_NAME);
        @$this->section->setExpiration(0);
        $credentialObject = array('credentials' => $credentials);
        $this->sessionsClient = new SessionsClient($credentialObject);
        $this->projectId = $projectId;
        $this->languageCode = $languageCode;

        // generate new session ID
        $this->session = $this->sessionsClient->sessionName($this->projectId, $this->getUserId());
    }

    /**
     * @return SessionSection
     */
    public function getSessionSection(): SessionSection
    {
        return $this->section;
    }

    /**
     * A Dialogflow session ID for an user
     *
     * @return string a unique id for the user
     */
    public function getUserId()
    {
        if (!$this->section->userId)
        {
            $this->section->userId = uniqid();
        }
        return $this->section->userId;
    }

    public function initFallback()
    {
        $this->section->messages = [];
    }

    public function appendMessage(DialogMessage $message)
    {
        $this->section->messages[] = $message;
    }

    public function getMessages()
    {
        return $this->section->messages;
    }

    /**
     * Creates a QueryInput for a message
     *
     * @param string $text message text
     * @param bool $isHidden show the message in the client
     * @return QueryInput
     */
    public function queryInput($text, $isHidden = FALSE)
    {
        // create text input
        $textInput = new TextInput();
        $textInput->setText($text);
        $textInput->setLanguageCode($this->languageCode);

        // create query input
        $queryInput = new QueryInput();
        $queryInput->setText($textInput);
        return $queryInput;
    }

    /**
     * Initializes the conversation by sending the WELCOME event query to Dialogflow
     *
     * @throws \Google\ApiCore\ApiException
     */
    public function initConversation()
    {
        $queryInput = new QueryInput();
        $queryInput->setEvent(new EventInput([
            'name' => 'WELCOME',
            'language_code' => $this->languageCode
        ]));

        $response = new DialogflowResponse($this->sessionsClient->detectIntent($this->session, $queryInput));
        $this->appendMessage(new DialogMessage($response->getFulfilmentText(), EDialogMessageType::RECEIVED));
    }

    /**
     * Input a user message text to the session messages
     *
     * @param string $text message text
     */
    public function inputMessage($text)
    {
        $this->appendMessage(new DialogMessage($text, EDialogMessageType::SENT));
    }

    /**
     * Sends a message query to Dialogflow
     *
     * @param QueryInput $queryInput query input containing the user message
     * @throws \Google\ApiCore\ApiException
     */
    public function sendMessage($queryInput)
    {
        // get response and relevant info
        $response = new DialogflowResponse($this->sessionsClient->detectIntent($this->session, $queryInput));
        $this->appendMessage(new DialogMessage($response->getFulfilmentText(), EDialogMessageType::RECEIVED));
    }

    /**
     * Deletes the session. Used to reset the chatbot messages
     */
    public function clearChat()
    {
        $this->section->remove();
    }
}