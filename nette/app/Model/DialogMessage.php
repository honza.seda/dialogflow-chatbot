<?php


namespace App\Model;


/**
 * Class DialogMessage
 *
 * object to store an individual message of the conversation
 *
 * @package App\Model
 */
class DialogMessage
{
    /**
     * @var string
     */
    private $text;
    /**
     * @var string
     */
    private $type;
    /**
     * @var bool
     */
    private $isHidden;

    public function __construct($text, $type, $isHidden = FALSE)
    {
        $this->text = $text;
        $this->type = $type;
        $this->isHidden = $isHidden;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->isHidden;
    }
}