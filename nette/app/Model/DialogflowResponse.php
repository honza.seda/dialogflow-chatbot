<?php


namespace App\Model;


/**
 * Class DialogflowResponse
 *
 * object to store the response from a query to Dialogflow
 *
 * @package App\Model
 */
class DialogflowResponse
{
    /**
     * @var string
     */
    private $queryText;
    /**
     * @var string
     */
    private $displayName;
    /**
     * @var float
     */
    private $confidence;
    /**
     * @var string
     */
    private $fulfilmentText;

    public function __construct($response)
    {
        $queryResult = $response->getQueryResult();
        $this->queryText = $queryResult->getQueryText();
        $intent = $queryResult->getIntent();
        $this->displayName = $intent->getDisplayName();
        $this->confidence = $queryResult->getIntentDetectionConfidence();
        $this->fulfilmentText = $queryResult->getFulfillmentText();
    }

    /**
     * @return string
     */
    public function getQueryText()
    {
        return $this->queryText;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return float
     */
    public function getConfidence()
    {
        return $this->confidence;
    }

    /**
     * @return string
     */
    public function getFulfilmentText()
    {
        return $this->fulfilmentText;
    }
}