<?php


namespace App\Components;


use App\Enum\EFlashMessageTypes;
use App\Model\DialogflowSession;
use Nette\Application\UI\Control;

class ChatFrame extends Control
{
    private $dialogflowSession;

    public function __construct($dialogflowSession)
    {
        $this->dialogflowSession = $dialogflowSession;
        if (empty($this->dialogflowSession->getMessages()))
        {
            try
            {
                $this->dialogflowSession->initConversation();
            }
            catch (\Exception $e)
            {
                $this->dialogflowSession->initFallback();
            }
        }
    }

    public function render()
    {
        if(empty($this->dialogflowSession->getMessages()))
        {
            $this->flashMessage('There was an error initializing the bot. Please try again later.', EFlashMessageTypes::ERROR);
        }

        $this->template->setFile(__DIR__ . '/ChatFrame.latte');

        $this->template->messages = $this->dialogflowSession->getMessages();

        $this->template->render();
    }

    public function handleNewMessage($inputText)
    {
        $this->dialogflowSession->inputMessage($inputText);
        $this->redrawControl('messages');
        $this->redrawControl('scrollToBottom');
    }

    public function handleSendMessage($inputText)
    {
        try
        {
            $query = $this->dialogflowSession->queryInput($inputText);
            $this->dialogflowSession->sendMessage($query);
        }
        catch (\Exception $e)
        {
            $this->flashMessage('There was an error communicating with the bot. Try again later.', EFlashMessageTypes::ERROR);
        }
        $this->redrawControl('frameFlashes');
        $this->redrawControl('messages');
        $this->redrawControl('scrollToBottom');
    }
}

interface IChatFrameFactory
{
    /**
     * @param $dialogflowSession DialogflowSession
     * @return ChatFrame
     */
    public function create($dialogflowSession);
}