<?php


namespace App\Enum;


class EDialogMessageType
{
    const SENT = 'sent';
    const RECEIVED = 'replies';
}