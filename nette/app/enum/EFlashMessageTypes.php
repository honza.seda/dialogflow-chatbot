<?php


namespace App\Enum;


class EFlashMessageTypes
{
    const SUCCESS = "primary";
    const ERROR = "error";
    const INFO = "info";
    const WARNING = "warning";

    public static $flashIcon = [
        self::SUCCESS => 'check',
        self::ERROR => 'exclamation-circle',
        self::INFO => 'info'
    ];

    public static function getFlashIcon($type): string
    {
        if (isset(self::$flashIcon[$type]))
        {
            return self::$flashIcon[$type];
        }

        return 'info';
    }
}