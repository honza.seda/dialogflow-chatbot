<?php


namespace App\Presenters;


use App\Components\IChatFrameFactory;
use App\Model\DialogflowSession;

class DefaultPresenter extends BasePresenter
{
    /**
     * @var IChatFrameFactory
     */
    private $chatFrameFactory;

    public function __construct(
        IChatFrameFactory $chatFrameFactory,
        DialogflowSession $dialogflowSession
    )
    {
        parent::__construct($dialogflowSession);
        $this->chatFrameFactory = $chatFrameFactory;
    }

    public function actionDefault()
    {

    }

    public function actionReset()
    {
        $this->dialogflowSession->clearChat();
        $this->redirect('Default:');
    }

    /**
     * @return \App\Components\ChatFrame
     */
    public function createComponentChatFrame()
    {
        return $this->chatFrameFactory->create($this->dialogflowSession);
    }
}