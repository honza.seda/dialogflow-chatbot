<?php


namespace App\Presenters;


use App\Model\DialogflowSession;
use Nette\Application\UI\Presenter;
use Tracy\Debugger;

abstract class BasePresenter extends Presenter
{
    /**
     * @var DialogflowSession
     */
    protected $dialogflowSession;

    public function __construct(DialogflowSession $dialogflowSession)
    {
        parent::__construct();
        $this->dialogflowSession = $dialogflowSession;
    }

    protected function startup()
    {
        parent::startup();
    }

    /**
     * Přidání flash message přes ajax rovnou zajistí překreslení controlu
     * @param string $message
     * @param string $type
     * @return mixed
     */
    public function flashMessage($message, string $type = 'info'): \stdClass
    {
        $flashMessage = parent::flashMessage($message, $type);

        if ($this->isAjax())
        {
            $this->redrawControl("flashMessages");
        }

        return $flashMessage;
    }
}